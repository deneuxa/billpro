require_relative './spec_helper'

describe BillPro do

  let(:billpro)        { get_billpro_client }
  let(:amt)            { 1234 } # even amount == successful transaction
  let(:cst)            { get_customer }
  let(:crd)            { get_card }
  let(:ip)             { '42.42.42.42' }
  let(:transaction_id) { '1234567890' }

  describe '::VERSION' do
    it 'should be a string' do
      expect(BillPro::VERSION).to be_a(String)
    end
  end

  describe '#authorize_capture_request' do
    context 'with correct parameters' do
      let(:request) do
        Ox.load(billpro.send(:authorize_capture_request, 'foo', amt, cst, crd, ip, 'EUR'))
      end

      it 'should include the amount' do
        expect(request.Request.Transaction.Amount.text).to eql('12.34')
      end
    end

    context 'with incorrect parameters' do
      it 'should raise with an invalid IP' do
        expect { billpro.send(:authorize_capture_request, 'foo', amt, cst, crd, 'BOGUS', 'EUR') }.
          to raise_error('Missing or invalid IP (BOGUS)')
      end

      it 'should raise with a missing amount' do
        expect { billpro.send(:authorize_capture_request, 'foo', nil, cst, crd, ip, 'EUR') }.
          to raise_error('Missing or invalid amount ()')
      end

      it 'should raise with an invalid currency' do
        expect { billpro.send(:authorize_capture_request, 'foo', amt, cst, crd, ip, 'LOL') }.
          to raise_error('Missing or invalid currency (LOL)')
      end
    end
  end

  describe '#authorize_capture', :vcr do
    context 'successful' do
      let(:response)  { billpro.authorize_capture('miaouw', amt, cst, crd, ip, 'EUR') }

      it 'should return a transaction object' do
        expect(response).to be_a(BillPro::Transaction)
      end

      it 'should return a numeric string as transaction ID' do
        expect(response.transaction_id.to_i).to be > 1000
      end

      it 'should return the reference we gave it' do
        expect(response.reference).to eql('miaouw')
      end
    end

    context 'failed' do
      let(:amt) { 1233 }

      it 'should return an error' do
        expect(billpro.authorize_capture('foo', amt, cst, crd, ip, 'EUR')).
          to be_a(BillPro::Error)
      end
    end
  end

  describe '#recur_request' do
    context 'with correct parameters' do
      let(:request) do
        Ox.load(billpro.send(:recur_request, 'foo', amt, transaction_id))
      end

      it 'should include the amount' do
        expect(request.Request.Transaction.Amount.text).to eql('12.34')
      end

      it 'should raise with a missing amount' do
        expect { billpro.send(:recur_request, 'foo', nil, transaction_id) }.
          to raise_error('Missing or invalid amount ()')
      end

      it 'should raise with an invalid transaction ID' do
        expect { billpro.send(:recur_request, 'foo', amt, nil) }.
          to raise_error("Missing or invalid transaction ID ()")
      end
    end
  end

  describe '#recur', :vcr do
    context 'successful' do
      let(:transaction)  { billpro.authorize_capture('miaouw', amt, cst, crd, ip, 'EUR') }
      let(:response)  { billpro.recur('miaouw', amt, transaction.transaction_id) }

      it 'should return a transaction object' do
        expect(response).to be_a(BillPro::Transaction)
      end

      it 'should return a numeric string as transaction ID' do
        expect(response.transaction_id.to_i).to be > 1000
      end

      it 'should return the reference we gave it' do
        expect(response.reference).to eql('miaouw')
      end
    end

    context 'failed' do
      let(:amt) { 1233 }
      let(:transaction)  { billpro.authorize_capture('miaouw', amt, cst, crd, ip, 'EUR') }

      it 'should return an error' do
        expect(billpro.recur('foo', amt, transaction.transaction_id)).
          to be_a(BillPro::Error)
      end
    end
  end

  describe '#create_customer_request' do
    context 'with incorrect parameters' do
      it 'should raise with an invalid IP' do
        expect { billpro.send(:create_customer_request, 'miaouw', cst, crd, 'BOGUS') }.
          to raise_error('Missing or invalid IP (BOGUS)')
      end
    end
  end

  describe '#create_customer', :vcr do
    context 'successful' do
      let(:response)  { billpro.create_customer('miaouw', cst, crd, ip) }

      it 'should return a customer created object' do
        expect(response).to be_a(BillPro::CustomerCreated)
      end

      it 'should return the reference we gave it' do
        expect(response.reference).to eql('miaouw')
      end
    end
  end

end
