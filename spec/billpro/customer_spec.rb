require_relative '../spec_helper'

describe BillPro::Customer do

  let(:customer) { get_customer }

  describe '#to_h' do
    it 'should return a correct hash' do
      expect(customer.to_h).to eql({
        Email:      'trotro@tv.com',
        Phone:      '42',
        FirstName:  'Jean',
        LastName:   'Valjean',
        Address:    '12 3/4 Street',
        PostCode:   '666',
        City:       'Derpville',
        State:      'N/A',
        Country:    'MU'
      })
    end
  end

end

