require_relative '../spec_helper'

describe BillPro::Card do

  let(:card) { get_card }

  describe '#to_h' do
    it 'should return a correct hash' do
      expect(card.to_h).to eql({
        CardNumber:   '4141414141414141',
        CardExpYear:  '2020',
        CardExpMonth: '01',
        CardCVV:      '666'
      })
    end
  end

end

