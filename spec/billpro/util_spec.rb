require_relative '../spec_helper'

describe BillPro::Util do

  describe '.camelize' do
    it 'should camelize a symbol' do
      expect(BillPro.camelize(:truten_frischen)).to eql(:TrutenFrischen)
    end
  end

  describe '.hash_to_xml' do
    let(:hsh)     { { a: { b: 'c' } } }
    let(:builder) { Builder::XmlMarkup.new }

    it 'should recursively generate the XML' do
      expect(BillPro.hash_to_xml('r', hsh, builder, { foo: 'bar' })).
        to eql('<r foo="bar"><a><b>c</b></a></r>')
    end
  end

end

