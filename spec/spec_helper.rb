require 'byebug'
require 'simplecov'
require 'vcr'

#
# Test credentials, gem is private, no big deal even if accidentally published.
#
TEST_ACCOUNT_ID   = '99308274'
TEST_ACCOUNT_AUTH = 'kLtasSdWIpqROANR'

SimpleCov.start do
  add_filter '/spec/'
end

VCR.configure do |config|
  config.cassette_library_dir       = 'spec/cassettes'
  config.hook_into                  :webmock
  config.default_cassette_options   = { record: :once }
  config.configure_rspec_metadata!
end

require(File.expand_path('../../lib/billpro', __FILE__))

#
# Helper methods
#
def get_card
  BillPro::Card.new do |c|
    c.pan         = '4141 4141 4141 4141'
    c.expiration  = Date.parse('2020-01-01')
    c.cvv         = '666'
  end
end

def get_customer
  BillPro::Customer.new do |c|
    c.email       = 'trotro@tv.com'
    c.phone       = '42'
    c.first_name  = 'Jean'
    c.last_name   = 'Valjean'
    c.address     = '12 3/4 Street'
    c.post_code   = '666'
    c.city        = 'Derpville'
    c.state       = 'N/A'
    c.country     = 'MU'
  end
end

def get_billpro_client
  BillPro.new(TEST_ACCOUNT_ID, TEST_ACCOUNT_AUTH)
end

