require 'billpro/common_fields'

class BillPro

  #
  # Encapsulates data relevant to a BillPro error
  #
  class Error < CommonFields
  end

end

