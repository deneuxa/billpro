require 'credit_card_validator'

class BillPro

  #
  # Encapsulates data relevant to a credit card
  #
  class Card

    prepend BillPro::EasyInit

    #
    # Credit card attributes
    #
    MANDATORY_ATTRIBUTES = [:pan, :expiration, :cvv]

    attr_accessor *MANDATORY_ATTRIBUTES

    #
    # Do all required fields have a value, and does the credit card number pass
    # basic validity checks ?
    #
    # @return [Boolean]
    #
    def valid?
      MANDATORY_ATTRIBUTES.all? { |attr| send(attr) } &&
        CreditCardValidator::Validator.valid?(pan) &&
        expiration.is_a?(Date) && cvv.is_a?(String) && cvv.length == 3
    end

    #
    # Returns a hash suitable for direct XML conversion
    #
    # @return [Hash]
    #
    def to_h
      'Card data is invalid!' unless valid?

      {
        CardNumber:   pan.gsub(/\s/, ''),
        CardExpMonth: '%02d' % expiration.month,
        CardExpYear:  expiration.year.to_s,
        CardCVV:      cvv
      }
    end

  end
end

