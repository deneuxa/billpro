class BillPro

  #
  # Encapsulates the required fields for the BillPro customer XML data
  #
  class Customer

    prepend BillPro::EasyInit

    #
    # The attributes that *must* have a value for the customer data, we don't
    # care about the other ones
    #
    MANDATORY_ATTRIBUTES = [:email, :phone, :first_name, :last_name, :address,
                            :city, :post_code, :state, :country]

    attr_accessor *MANDATORY_ATTRIBUTES

    # @todo Check country code

    #
    # Do all the required fields have a value ?
    #
    # @return [Boolean]
    #
    def valid?
      MANDATORY_ATTRIBUTES.all? do |attr|
        val = send(attr)
        val && val.is_a?(String)
      end
    end

    #
    # Returns a hash suitable for direct XML conversion
    #
    # @return [Hash]
    #
    def to_h
      raise 'Customer data is invalid!' unless valid?

      MANDATORY_ATTRIBUTES.inject({}) do |h, attr|
        h[BillPro.camelize(attr)] = send(attr); h
      end
    end

  end
end

