require 'billpro/common_fields'

class BillPro

  #
  # Encapsulates details of a BillPro transaction
  #
  class Transaction < CommonFields
  end

end
