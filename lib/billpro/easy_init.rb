class BillPro

  #
  # Initialization syntactic sugar
  #
  module EasyInit

    #
    # Yields `self` if required
    #
    def initialize
      yield(self) if block_given?
      super
    end

  end
end

