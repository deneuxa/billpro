class BillPro

  #
  # Utility methods module
  #
  module Util

    #
    # Camelize a `Symbol` or a `String` and returns a `Symbol`
    #
    # @param sym [Symbol]
    # @return [Symbol]
    #
    def camelize(sym)
      sym.to_s.split('_').map(&:capitalize).join.to_sym
    end

    #
    # Generates an XML string from a root node tag and a hash.
    #
    # @param root [String] The root node name
    # @param val [Hash, String] If the parameter is a `Hash` the function
    #   is called recursively, otherwise it renders a text node
    # @param builder [Builder::XmlMarkup]  The XML builder to use for rendering
    # @param attrs [Hash] The attributes to set on the root node
    #
    def hash_to_xml(root, val, builder, attrs = {})
      if val.is_a?(Hash)
        builder.tag!(root, attrs) do
          val.each do |k, v|
            hash_to_xml(k, v, builder)
          end
        end
      else
        builder.tag!(root) { builder.text!(val) }
      end
    end

  end
end

