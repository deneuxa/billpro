class BillPro

  #
  # Common fields for both `BillPro::Transaction` and `BillPro::Error`
  #
  class CommonFields

    prepend BillPro::EasyInit

    attr_accessor :response_type, :response_code, :response_desc, :status_code, :status_desc,
      :raw_response, :transaction_id, :reference

    def parse_response(response)

      self.reference      = response.Reference.text
      self.transaction_id = response.TransactionID.text
      self.response_type  = response.type
      self.response_code  = response.ResponseCode.text
      self.response_desc  = response.Description.text

      if self.response_type == 'ACK'
        self.status_code    = response.StatusCode.text
        self.status_desc    = response.StatusDescription.text
      end

    end

  end
end
