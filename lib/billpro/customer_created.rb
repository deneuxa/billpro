class BillPro

  #
  # Encapsulates details of a Customer creation transaction
  #
  class CustomerCreated

    prepend BillPro::EasyInit

    attr_accessor :response_type, :response_code, :response_desc, :raw_response,
      :customer_id, :reference

    def parse_response(response)

      self.reference      = response.Reference.text
      self.response_type  = response.type
      self.response_code  = response.ResponseCode.text
      self.response_desc  = response.Description.text
      self.customer_id  = response.CustomerID.text

    end

  end

end
