require 'rest-client'
require 'builder'
require 'bigdecimal'
require 'ox'

require 'billpro/version'
require 'billpro/easy_init'
require 'billpro/customer'
require 'billpro/card'
require 'billpro/transaction'
require 'billpro/customer_created'
require 'billpro/error'
require 'billpro/util'

#
# BillPro top-level module
#
class BillPro

  # The default XML service endpoint
  GATEWAY = 'https://gateway.billpro.com/'

  extend BillPro::Util

  attr_accessor :account_id, :account_auth

  #
  # Initializes a `BillPro` client instance given authentication credentials.
  #
  # @param account_id [String]
  # @param account_auth [String]
  #
  def initialize(account_id, account_auth)
    @account_id   = account_id
    @account_auth = account_auth
  end

  #
  # Performs an 'AuthorizeCapture' request
  #
  # @param reference [String] The client reference
  # @param cents [Fixnum] The integer amount of cents to harge
  # @param customer [BillPro::Customer] The customer being charged
  # @param card [BillPro::Card] The card to charge
  # @param ip [String] The customer IP
  # @param currency [String]
  #
  def authorize_capture(reference, cents, customer, card, ip, currency)
    do_request(authorize_capture_request(reference, cents, customer, card, ip, currency))
  end

  #
  # Performs a 'Recur' request
  #
  # @param reference [String] The client reference
  # @param cents [Fixnum] The integer amount of cents to charge
  # @param transaction_id [String] The id of a previous successful AuthorizeCapture transaction
  #
  def recur(reference, cents, transaction_id)
    do_request(recur_request(reference, cents, transaction_id))
  end

  #
  # Performs a 'CustomerCreate' request
  #
  # @param reference [String] The client reference
  # @param customer [BillPro::Customer] The customer to create
  # @param card [BillPro::Card] The card associated
  # @param ip [String] The customer IP
  #
  def create_customer(reference, customer, card, ip)
    do_request(create_customer_request(reference, customer, card, ip))
  end

  #
  # Performs the actual request against the BillPro gateway
  #
  # @param req [String] the XML payload
  # @return [BillPro::Transaction]
  #
  def do_request(req)
    raw_xml   = RestClient.post(GATEWAY, req)
    response  = Ox.load(raw_xml).root

    res = if response[:type] == 'ACK' && response.ResponseCode.text == '100'
            BillPro::Transaction.new
          elsif response[:type] == 'ACK' && response.ResponseCode.text == '110'
            BillPro::CustomerCreated.new
          else
            BillPro::Error.new
          end

    res.parse_response(response)
    res.raw_response = raw_xml

    res
  end

  private

  #
  # Returns an 'AuthorizeCapture' request XML.
  #
  # @param reference [String] The client reference
  # @param cents [Fixnum] The integer amount of cents to harge
  # @param customer [BillPro::Customer] The customer being charged
  # @param card [BillPro::Card] The card to charge
  # @param ip [String] The customer IP
  # @param currency [String]
  #
  def authorize_capture_request(reference, cents, customer, card, ip, currency)
    unless ip && ip =~ /\A([0-9]{1,3}\.){3}[0-9]{1,3}\z/
      raise "Missing or invalid IP (#{ip})"
    end

    unless cents && cents.is_a?(Fixnum) && cents >= 1 && cents < 1_000_000
      raise "Missing or invalid amount (#{cents})"
    end

    unless currency && currency == 'EUR'
      raise "Missing or invalid currency (#{currency})"
    end

    raise "Missing customer" unless customer
    raise "Missing card" unless card

    tx_h  = {}

    tx_h.merge!(customer.to_h)
    tx_h.merge!(card.to_h)

    tx_h[:Amount]     = (BigDecimal(cents.to_s) / 100).to_s('f')
    tx_h[:Reference]  = reference
    tx_h[:Currency]   = currency
    tx_h[:IPAddress]  = ip


    req_h = get_auth_hash
    req_h[:Transaction] = tx_h

    BillPro.hash_to_xml('Request', req_h, get_builder, type: 'AuthorizeCapture')
  end

  #
  # Returns a 'Recur' request XML.
  #
  # @param reference [String] The client reference
  # @param cents [Fixnum] The integer amount of cents to harge
  # @param transaction_id [Fixnum] The id of a previous successful AuthorizeCapture transaction
  #
  def recur_request(reference, cents, transaction_id)
    unless cents && cents.is_a?(Fixnum) && cents >= 1 && cents < 1_000_000
      raise "Missing or invalid amount (#{cents})"
    end

    unless transaction_id && transaction_id.is_a?(String) && transaction_id.to_i > 1000
      raise "Missing or invalid transaction ID (#{transaction_id})"
    end

    tx_h  = {}

    tx_h[:Amount]     = (BigDecimal(cents.to_s) / 100).to_s('f')
    tx_h[:Reference]  = reference
    tx_h[:TransactionID]  = transaction_id

    req_h = get_auth_hash
    req_h[:Transaction] = tx_h

    BillPro.hash_to_xml('Request', req_h, get_builder, type: 'Recur')
  end

  #
  # Returns a 'CreateCustomer' request XML.
  #
  # @param reference [String] The client reference
  # @param customer [BillPro::Customer] The customer to create
  # @param card [BillPro::Card] The card associated
  # @param ip [String] The customer IP
  #
  def create_customer_request(reference, customer, card, ip)
    unless ip && ip =~ /\A([0-9]{1,3}\.){3}[0-9]{1,3}\z/
      raise "Missing or invalid IP (#{ip})"
    end

    tx_h  = {}

    tx_h.merge!(customer.to_h)
    tx_h.merge!(card.to_h)

    tx_h[:Reference]  = reference
    tx_h[:IPAddress]  = ip

    req_h = get_auth_hash
    req_h[:Customer] = tx_h

    BillPro.hash_to_xml('Request', req_h, get_builder, type: 'CustomerCreate')
  end

  #
  # Returns a new `Builder::XmlMarkup` instance
  #
  # @return [Builder::XmlMarkup]
  #
  def get_builder
    bld = Builder::XmlMarkup.new
    bld.instruct!
    bld
  end

  #
  # Returns the attributes necessary to authenticate the request as a `Hash`
  # that can be directly merged into the request data hash.
  #
  # @return [Hash]
  #
  def get_auth_hash
    { AccountID: @account_id, AccountAuth: @account_auth }
  end

end
