# coding: utf-8

lib = File.expand_path('../lib/', __FILE__)
$:.unshift lib unless $:.include?(lib)

require 'billpro/version'

Gem::Specification.new do |s|
  s.name        = 'billpro'
  s.version     = BillPro::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ['David FRANCOIS']
  s.email       = ['david@fr.anco.is']
  s.homepage    = 'http://fr.anco.is'
  s.summary     = 'BillPro API wrapper'
  s.description = 'Wraps the BillPro API for easy consumption'
  s.licenses    = ['MIT']

  s.required_rubygems_version = '>= 1.3.6'

  s.add_dependency 'rest-client',           '~> 1.8'
  s.add_dependency 'ox',                    '~> 2.4'
  s.add_dependency 'builder',               '~> 3.2'
  s.add_dependency 'credit_card_validator', '~> 1.3'

  s.add_development_dependency 'webmock',   '~> 2.1'
  s.add_development_dependency 'byebug',    '~> 9.0'
  s.add_development_dependency 'rspec',     '~> 3.4'
  s.add_development_dependency 'vcr',       '~> 3.0'
  s.add_development_dependency 'rake',      '~> 10.4'
  s.add_development_dependency 'yard',      '~> 0.8'
  s.add_development_dependency 'simplecov', '~> 0.11'

  s.files = Dir.glob('lib/**/*') + %w(LICENSE README.md)

  s.require_path = 'lib'
end

